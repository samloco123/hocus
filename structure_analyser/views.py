from cProfile import label
from distutils.command.upload import upload
from django.shortcuts import render, redirect, get_object_or_404
from .forms import *
from .models import *
import msaf
import librosa
import os, shutil
from pathlib import Path
from django.http import JsonResponse
from django.db.models import Q

def index(request):
    return render(request, 'structure_analyser/homepage.html')

def structure_analysis(request):
    form = UploadForm(request.POST or None, request.FILES or None)
    if request.method == 'POST':
        if form.is_valid():
            upload = form.save(commit=False)
            # y, sr = librosa.load(upload.audio_file, duration=120)
            # onset_env = librosa.onset.onset_strength(y=y, sr=sr)
            # upload.tempo = librosa.beat.tempo(onset_envelope=onset_env, sr=sr)
            upload.tempo = 140
            upload.save()
            boundaries, labels = msaf.process(upload.audio_file.path, feature=upload.audio_features, boundaries_id=upload.segmentation_method, labels_id=upload.labelling_method)            
            #os.remove("./.features_msaf_tmp.json")
            #shutil.rmtree(os.path.join(Path(__file__).resolve().parent.parent) + '/media/estimations')
            # shutil.rmtree(os.path.join(Path(__file__).resolve().parent.parent) + '/estimations')
            #os.remove(upload.audio_file.path)
            upload2 = get_object_or_404(Upload, pk=upload.pk)
            form = UploadForm(request.POST, instance=upload2)
            upload2 = form.save(commit=False)
            upload2.set_annotations( [list(boundaries), labels, [0,0]] )
            upload2.save()
            return redirect('music_structure', pk=upload2.pk)
    context = {
        'form': form,
    }
    return render(request, 'structure_analyser/analysis.html', context)

def music_structure(request, pk):
    upload = get_object_or_404(Upload, pk=pk)
    [boundaries, labels, rF] = upload.get_annotations()
    # if request.method == 'POST':
    #     # update rating
    #     # collect new rating
    #     rating = request.POST.get('val')
    #     # get upload object
    #     form = UploadForm(instance=upload)
    #     upload = form.save(commit=False)
    #     # update rating and annotations
    #     upload.rating = int((rating+ratingFraction[0]) / (ratingFraction[1]+1))
    #     upload.set_annotations([boundaries, labels, [upload.rating + ratingFraction[0], (ratingFraction[1] + 1)]] )
    #     upload.save()
    #    return redirect('music_structure', pk=upload.pk)
    return render(request, 'structure_analyser/music_structure.html', {'music':upload, 'bounds':boundaries, 'labels':labels})

def rate_analysis(request):
    if request.method == 'POST':
        el_id = request.POST.get('el_id')
        val = request.POST.get('val')
        obj = get_object_or_404(Upload, pk=el_id)
        
        [bs, ls, rF] = obj.get_annotations()
        print(int(val)+int(rF[0]))
        print((rF[1]+1))
        obj.rating = (int(val)+int(rF[0])) // (int(rF[1])+1)
        obj.set_annotations( [bs, ls, [(int(val)+int(rF[0])), (int(rF[1])+1)]] )
        obj.save()
        return JsonResponse({'success':'true', 'score': val}, safe=False)
    return JsonResponse({'success':'false'})

def uploads_library(request):
    uploads = reversed(Upload.objects.filter(upload_date__lte=timezone.now()).order_by('upload_date'))
    return render(request, 'structure_analyser/library.html', {'uploads': uploads})

def search(request):
    query = request.GET.get('q')
    if query:
        uploads = Upload.objects.filter(title__contains=query)
    else:
        uploads = reversed(Upload.objects.filter(upload_date__lte=timezone.now()).order_by('upload_date'))
    return render(request, 'structure_analyser/search_results.html', {'uploads': uploads})