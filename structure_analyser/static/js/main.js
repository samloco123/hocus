var audioTrack = WaveSurfer.create({
	container: '.audio',
    waveColor: '#ffb491',
    progressColor: '#ec4b00',
    barWidth: 2.2,
    barMinHeight: 0.3,
    cursorWidth: 0,
	plugins: [
        WaveSurfer.regions.create({})
    ]
});

audioTrack.load(audio_url);
const playBtn = document.querySelector(".play-btn");
const regionsControl = document.getElementById("region-controls");

// labels and colour dictionary
const labelDict = ['A','B','C','D','','E','F'];
const colDict = ['60','120','180','240','','300','360'];

// adding regions
for (let i=labels.length-1; i>0; i--) {
	if (labels[i] != 4.0) {
		audioTrack.addRegion({
			id: labelDict[labels[i] | 0], 
			start: bounds[i], 
			end: bounds[i+1], 
			color: 'hsla(' + (colDict[labels[i] | 0]) + ', 100%, 50%, 0.4)', 
			resize: false, 
			drag: false
		});
	}
}
// disabling user-option to create new regions
audioTrack.disableDragSelection();

console.log(audioTrack.regions)

// adding play/preview buttons for the regions
var arr1 = [...new Set(labels)];
const uniqLabels = arr1.filter(function(x) {
    return x !== 4.0;
});
for (let i=0; i<uniqLabels.length; i++) {
	let region = Object.values(audioTrack.regions.list)[i];
	regionsControl.innerHTML += `<br>
                                <button class="btn btn-primary" data-action="play-region-${(1+i).toString()}">
									<i class="glyphicon glyphicon-play"></i>
									Play Section ${region.id}
								</button>
								<br>`		
}

// adding play/preview functionality to buttons
for (let i=0; i<uniqLabels.length; i++) {
	document.querySelector(
		`[data-action="play-region-${(1+i).toString()}"]`
	).addEventListener('click', function() {
		let region = Object.values(audioTrack.regions.list)[i];
		region.play();
		playBtn.classList.add("playing");
	});
}

// play/pause button event listener
playBtn.addEventListener("click", () => {
    audioTrack.playPause();
    if (audioTrack.isPlaying()) {
        playBtn.classList.add("playing");
      } else {
        playBtn.classList.remove("playing");
      }
  });
audioTrack.on('finish', function () {
    playBtn.classList.remove("playing");
});
audioTrack.on('pause', function () {
    playBtn.classList.remove("playing");
});

// get all stars
const one = document.getElementById('first');
const two = document.getElementById('second');
const three = document.getElementById('third');
const four = document.getElementById('fourth');
const five = document.getElementById('fifth');

// get the form, confirm-box and csrf token
const form = document.querySelector('.rate-form')
const confirmBox = document.getElementById('confirm-box')
const csrf = document.getElementsByName('csrfmiddlewaretoken')

const handleStarSelect = (size) => {
    const children = form.children
    //console.log(children[0])
    for (let i=0; i < children.length; i++) {
        if(i <= size) {
            children[i].classList.add('checked')
        } else {
            children[i].classList.remove('checked')
        }
    }
}

const handleSelect = (selection) => {
    switch(selection){
        case 'first': {

            handleStarSelect(1)
            return
        }
        case 'second': {
            handleStarSelect(2)
            return
        }
        case 'third': {
            handleStarSelect(3)
            return
        }
        case 'fourth': {
            handleStarSelect(4)
            return
        }
        case 'fifth': {
            handleStarSelect(5)
            return
        }
        default: {
            handleStarSelect(0)
        }
    }
}

const getNumericValue = (stringValue) =>{
    let numericValue;
    if (stringValue === 'first') {
        numericValue = 1
    } 
    else if (stringValue === 'second') {
        numericValue = 2
    }
    else if (stringValue === 'third') {
        numericValue = 3
    }
    else if (stringValue === 'fourth') {
        numericValue = 4
    }
    else if (stringValue === 'fifth') {
        numericValue = 5
    }
    else {
        numericValue = 0
    }
    return numericValue
}

if (one) {
    const arr = [one, two, three, four, five]

    arr.forEach(item=> item.addEventListener('mouseover', (event)=>{
        handleSelect(event.target.id)
    }))

    arr.forEach(item=> item.addEventListener('click', (event)=>{
        // value of the rating not numeric
        const val = event.target.id

		let isSubmit = false
        form.addEventListener('submit', e=>{
            e.preventDefault()
            if (isSubmit) {
                return
            }
            isSubmit = true
            // picture id
            const id = e.target.id
            // value of the rating translated into numeric
            const val_num = getNumericValue(val)
			console.log(id)
			console.log(val_num)

			console.log(csrf[0].value)
            $.ajax({
                type: 'POST',
                url: '/rate/',
                data: {
                    'csrfmiddlewaretoken': csrf[0].value,
                    'el_id': id,
                    'val': val_num,
                },
                success: function(response){
                    console.log(response)
                    confirmBox.innerHTML = `<h1>Successfully rated with ${response.score}</h1>`
                },
                error: function(error){
                    console.log(error)
                    confirmBox.innerHTML = '<h1>Ups... something went wrong</h1>'
                }
            })
        })
    }))
}