from django import forms
from .models import *

class UploadForm(forms.ModelForm):
    class Meta:
        model = Upload
        fields = ('title','audio_file','genre','segmentation_method','labelling_method','audio_features',)