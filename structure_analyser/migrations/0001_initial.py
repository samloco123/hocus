# Generated by Django 4.0.3 on 2022-04-06 03:44

import django.core.validators
from django.db import migrations, models
import django.utils.timezone
import structure_analyser.validators


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Upload',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200)),
                ('audio_file', models.FileField(upload_to='audio_uploads/', validators=[structure_analyser.validators.validate_file_extension])),
                ('genre', models.CharField(choices=[('POP', 'Pop'), ('ROCK', 'Rock'), ('HIPHOP', 'Hip Hop'), ('LATIN', 'Latin'), ('EDM', 'Dance/Electronic'), ('R&B', 'R&B'), ('COUNTRY', 'Country'), ('FOLK', 'Folk/Acoustic'), ('CLASSICAL', 'Classical'), ('JAZZ', 'Jazz'), ('METAL', 'Metal'), ('EASYL', 'Easy Listening'), ('NEWAGE', 'New Age'), ('BLUES', 'Blues'), ('WORLD', 'World/Traditional')], max_length=64, null=True)),
                ('segmentation_method', models.CharField(choices=[('foote', 'Checkerboard Kernel'), ('cnmd', 'Convex NMF'), ('scluster', 'Laplacian Segmentation'), ('olda', 'Ordinal LDA'), ('sf', 'Structural Features'), ('vmp', 'Variable Markov Oracle')], default='foote', max_length=8)),
                ('labelling_method', models.CharField(choices=[('cnmf', 'Convex NMF'), ('scluster', 'Laplacian Segmentation'), ('vmo', 'Variable Markov Oracle'), ('fmc2d', '2D-Fourier Magnitude Coefficients')], max_length=8)),
                ('audio_features', models.CharField(choices=[('cqt', 'Constant-Q Transform'), ('mfcc', 'Mel-Frequency Cepstral Coefficients'), ('pcp', 'Pitch Class Profiles'), ('tempogram', 'Tempogram'), ('tonnetz', 'Tonal Centroids')], max_length=9)),
                ('tempo', models.IntegerField(max_length=4)),
                ('annotations', models.CharField(max_length=500)),
                ('rating', models.IntegerField(default=0, validators=[django.core.validators.MaxValueValidator(5), django.core.validators.MinValueValidator(0)])),
                ('upload_date', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
    ]
