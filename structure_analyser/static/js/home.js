$(document).ready(function () {
    $(window).scroll(function () {
        $('.header').toggleClass("header-scroll", ($(window).scrollTop() > 30));
        $('.links').toggleClass("links-scroll", ($(window).scrollTop() > 30));
        $('.btn').toggleClass("btn-scroll", ($(window).scrollTop() > 30));
        $('.srch').toggleClass("srch-scroll", ($(window).scrollTop() > 30));
     });
});