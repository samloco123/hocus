from django.db import models
from django.utils import timezone
from django.core.validators import MaxValueValidator, MinValueValidator
from .validators import validate_file_extension
from .defaults import *
import json

class Upload(models.Model):
    GENRE_CHOICES = [
        ('POP', 'Pop'),
        ('ROCK', 'Rock'),
        ('HIPHOP', 'Hip Hop'),
        ('LATIN', 'Latin'),
        ('EDM', 'Dance/Electronic'),
        ('R&B', 'R&B'),
        ('COUNTRY', 'Country'),
        ('FOLK', 'Folk/Acoustic'),
        ('CLASSICAL', 'Classical'),
        ('JAZZ', 'Jazz'),
        ('METAL', 'Metal'),
        ('EASYL', 'Easy Listening'),
        ('NEWAGE', 'New Age'),
        ('BLUES', 'Blues'),
        ('WORLD', 'World/Traditional'),
    ]
    BALGORITHM_CHOICES = [
        ('foote', 'Checkerboard Kernel'),
        ('cnmd', 'Convex NMF'),
        ('scluster', 'Laplacian Segmentation'),
        ('olda', 'Ordinal LDA'),
        ('sf', 'Structural Features'),
        ('vmp', 'Variable Markov Oracle'),
    ]
    LALGORITHM_CHOICES = [
        ('cnmf', 'Convex NMF'),
        ('scluster', 'Laplacian Segmentation'),
        ('vmo', 'Variable Markov Oracle'),
        ('fmc2d', '2D-Fourier Magnitude Coefficients'),
    ]
    FEATURES_CHOICES = [
        ('cqt', 'Constant-Q Transform'),
        ('mfcc', 'Mel-Frequency Cepstral Coefficients'),
        ('pcp', 'Pitch Class Profiles'),
        ('tempogram', 'Tempogram'),
        ('tonnetz', 'Tonal Centroids'),
    ]

    title = models.CharField(max_length=200)
    audio_file = models.FileField(upload_to='audio_uploads/', validators=[validate_file_extension])
    genre = models.CharField(max_length=64, choices=GENRE_CHOICES, null=True)

    segmentation_method = models.CharField(max_length=8, choices=BALGORITHM_CHOICES)
    labelling_method = models.CharField(max_length=8, choices=LALGORITHM_CHOICES)
    audio_features = models.CharField(max_length=9, choices=FEATURES_CHOICES)
    
    tempo = models.IntegerField(null=True)
    annotations = models.CharField(max_length=500, null=True)
    rating = models.IntegerField(default=0,
        validators=[
            MaxValueValidator(5),
            MinValueValidator(0)
        ] 
    )
    upload_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        if self.title =='':
            return str(self.pk)
        return self.title
    
    def set_annotations(self, x):
        self.annotations = json.dumps(x)

    def get_annotations(self):
        return json.loads(self.annotations)