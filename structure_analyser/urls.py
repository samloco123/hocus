from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('structure_analysis/', views.structure_analysis, name='structure_analysis'),
    path('structure_analysis/<int:pk>/', views.music_structure, name='music_structure'),
    path('library/', views.uploads_library, name='uploads_library'),
    path('search/', views.search, name='search_results'),
]